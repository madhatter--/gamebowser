# README #


### Description ###

Gamebowser is an online videogame rental / purchase store for CPSC 304.

### Technology Stack ###

AngularJS, PHP, MySQL

### Running instructions on Mac OSX for MAMP and Sequel Pro ###
Make sure your Mysql service is running:

/usr/local/bin/mysql.server start

MAMPPRO: 

- Make sure your project folder is in Mamp/htdocs
- Run MAMPPRO with default settings (Apache http port: 80, Apache https port: 443, MySQL port: 3306)
- Start Apache server

Sequel Pro:

- Use Socket connection
    Name: localhost
    Username: root
    Password: root
    Database:
    Socket: /Applications/MAMP/tmp/mysql/mysql.sock

- Connect with Sequel Pro
- Open browser: http://localhost/Gamebowser/index.html#/. 

### Owners ###

Hao Yang, Muzill Wahid, Joanna Leung, Daisy Tse

### Contributions ###

This project is based on the following seed project:
http://www.angularcode.com/user-authentication-using-angularjs-php-mysql/