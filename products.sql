CREATE DATABASE products;


USE products;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
SET foreign_key_checks=0;

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `year` year(4) NOT NULL,
  `company` varchar(100) NOT NULL,
  `rating` int(2) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `platform` varchar(50) NOT NULL,
  `players` int(2) NOT NULL,
  `stock` int(11) NOT NULL,
  `copiessold` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=951 ;

INSERT INTO `products` (`id`, `name`, `price`, `year`, `company`, `rating`, `genre`, `platform`, `players`, `stock`, `copiessold`) VALUES
(138, 'Metal Gear Solid 4   ', 25, 2005, 'Kojima', 10, 'Adventure', 'PS4', 1, 10, 100),
(248, 'Little Big Planet   ', 35, 2007, 'Studios',8, 'Adventure', 'PS4', 4, 10, 90),
(318, 'Halo   ', 45, 2009, 'Microsoft', 9, 'Shooter', 'XBOX', 2, 10, 80),
(432, 'Grand Theft Auto   ', 55, 2011, 'Rockstar', 10, 'Shooter', 'XBOX', 1, 20, 40),
(448, 'Diablo   ', 65, 2012, 'Blizzard', 4, 'RPG', 'PC', 1, 10, 20);

CREATE TABLE IF NOT EXISTS `shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `year` year(4) NOT NULL,
  `company` varchar(100) NOT NULL,
  `rating` int(2) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `platform` varchar(50) NOT NULL,
  `players` int(2) NOT NULL,
  `stock` int(11) NOT NULL,
  `copiessold` int(10) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Purchase',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=951 ;

INSERT INTO `shop` (`id`, `name`, `price`, `year`, `company`, `rating`, `genre`, `platform`, `players`, `stock`, `copiessold`, `status`) VALUES
(318, 'Halo   ', 45, 2009, 'Microsoft', 9, 'Shooter', 'XBOX', 2, 10, 80, 'Purchase'),
(432, 'Grand Theft Auto   ', 55, 2011, 'Rockstar', 10, 'Shooter', 'XBOX', 1, 20, 40, 'Rent'),
(448, 'Diablo   ', 65, 2012, 'Blizzard', 4, 'RPG', 'PC', 1, 10, 20, 'Purchase'),
(666,'Devil May Cry DmC', 35, 2014, 'Ninja Theory', 9, 'Action', 'PS4', 3, 10, 100, 'Purchase'),
(1337, 'CS:GO   ', 20, 2014, 'Valve',8, 'Shooter', 'PC', 4, 10, 90, 'Rent');

CREATE TABLE IF NOT EXISTS `pastshop` (
  `email` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `cardtype` varchar(40) NOT NULL,
  `creditcardname` varchar(100) NOT NULL,
  `creditcardnumber` int(40) NOT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `year` year(4) NOT NULL,
  `company` varchar(100) NOT NULL,
  `rating` int(2) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `platform` varchar(50) NOT NULL,
  `players` int(2) NOT NULL,
  `stock` int(11) NOT NULL,
  `copiessold` int(10) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Purchase',
  PRIMARY KEY (`email`, `password`, `id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=951 ;

CREATE TABLE IF NOT EXISTS `wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `year` year(4) NOT NULL,
  `company` varchar(100) NOT NULL,
  `rating` int(2) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `platform` varchar(50) NOT NULL,
  `players` int(2) NOT NULL,
  `stock` int(11) NOT NULL,
  `copiessold` int(10) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Purchase',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=951 ;

INSERT INTO `wishlist` (`id`, `name`, `price`, `year`, `company`, `rating`, `genre`, `platform`, `players`, `stock`, `copiessold`, `status`) VALUES
(138, 'Metal Gear Solid 4   ', 25, 2005, 'Kojima', 10, 'Adventure', 'PS4', 1, 10, 100, 'Purchase'),
(248, 'Little Big Planet   ', 35, 2007, 'Studios',8, 'Adventure', 'PS4', 4, 10, 90, 'Rent'),
(1337, 'CS:GO   ', 20, 2014, 'Valve',8, 'Shooter', 'PC', 4, 10, 90, 'Rent');

CREATE TABLE IF NOT EXISTS `pastwishlist` (
  `email` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `year` year(4) NOT NULL,
  `company` varchar(100) NOT NULL,
  `rating` int(2) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `platform` varchar(50) NOT NULL,
  `players` int(2) NOT NULL,
  `stock` int(11) NOT NULL,
  `copiessold` int(10) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Purchase',
  PRIMARY KEY (`email`, `password`, `id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=951 ;

INSERT INTO `pastwishlist` (`email`, `password`, `id`,`name`, `price`, `year`, `company`, `rating`, `genre`, `platform`, `players`, `stock`, `copiessold`, `status`)VALUES
('a@a', 'a', 138,'Metal Gear Solid 4', 25, 2005, 'Kojima', 10, 'Adventure', 'PS4', 1, 10, 100, 'Purchase'),
('s@s', 's', 248,'Little Big Planet', 35, 2007, 'Studios',8, 'Adventure', 'PS4', 4, 10, 90, 'Rent'),
('s@s', 's', 666,'Devil May Cry DmC', 35, 2014, 'Ninja Theory', 9, 'Action', 'PS4', 3, 10, 100, 'Purchase');

CREATE TABLE IF NOT EXISTS `checkout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `year` year(4) NOT NULL,
  `company` varchar(100) NOT NULL,
  `rating` int(2) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `platform` varchar(50) NOT NULL,
  `players` int(2) NOT NULL,
  `stock` int(11) NOT NULL,
  `copiessold` int(10) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Purchase',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=951 ;