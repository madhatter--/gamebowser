<?php
require '.././libs/Slim/Slim.php';
require_once 'dbHelper.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app = \Slim\Slim::getInstance();
$db = new dbHelper();

/**
 * Database Helper Function templates
 */
/*
select(table name, where clause as associative array)
insert(table name, data as associative array, mandatory column names as array)
update(table name, column names as associative array, where clause as associative array, required columns as array)
delete(table name, where clause as array)
*/


// Products
$app->get('/products', function() { 
    global $db;
    $rows = $db->select("products","id,name,price,year,company,rating,genre,platform,players,stock,copiessold",array());
    echoResponse(200, $rows);
});

$app->post('/products', function() use ($app) { 
    $data = json_decode($app->request->getBody());
    $mandatory = array('name');
    global $db;
    $rows = $db->insert("products", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product added successfully.";
    echoResponse(200, $rows);
});

$app->put('/products/:id', function($id) use ($app) { 
    $data = json_decode($app->request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("products", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product information updated successfully.";
    echoResponse(200, $rows);
});

$app->delete('/products/:id', function($id) { 
    global $db;
    $rows = $db->delete("products", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Product removed successfully.";
    echoResponse(200, $rows);
});

// Shopping Cart
$app->get('/shop', function() { 
    global $db;
    $rows = $db->select("shop","id,name,price,year,company,rating,genre,platform,players,stock,copiessold,status",array());
    echoResponse(200, $rows);
});

$app->post('/shop', function() use ($app) { 
    $data = json_decode($app->request->getBody());
    $mandatory = array('name');
    global $db;
    $rows = $db->insert("shop", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product added successfully.";
    echoResponse(200, $rows);
});

$app->put('/shop/:id', function($id) use ($app) { 
    $data = json_decode($app->request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("shop", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product information updated successfully.";
    echoResponse(200, $rows);
});

$app->delete('/shop/:id', function($id) { 
    global $db;
    $rows = $db->delete("shop", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Product removed successfully.";
    echoResponse(200, $rows);
});

// Wishlist
$app->get('/wishlist', function() { 
    global $db;
    $rows = $db->select("wishlist","id,name,price,year,company,rating,genre,platform,players,stock,copiessold,status",array());
    echoResponse(200, $rows);
});

$app->post('/wishlist', function() use ($app) { 
    $data = json_decode($app->request->getBody());
    $mandatory = array('name');
    global $db;
    $rows = $db->insert("wishlist", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product added successfully.";
    echoResponse(200, $rows);
});

$app->put('/wishlist/:id', function($id) use ($app) { 
    $data = json_decode($app->request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("wishlist", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product information updated successfully.";
    echoResponse(200, $rows);
});

$app->delete('/wishlist/:id', function($id) { 
    global $db;
    $rows = $db->delete("wishlist", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Product removed successfully.";
    echoResponse(200, $rows);
});

// Pastwishlist
$app->get('/pastwishlist', function() {
    global $db;
    $rows = $db->select("pastwishlist","email,password,name,price,year,company,rating,genre,platform,players,stock,copiessold,status",array());
    echoResponse(200, $rows);
});

$app->post('/pastwishlist', function() use ($app) {
    $data = json_decode($app->request->getBody());
    $mandatory = array('name');
    global $db;
    $rows = $db->insert("pastwishlist", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product added successfully.";
    echoResponse(200, $rows);
});

$app->put('/pastwishlist/:id', function($id) use ($app) {
    $data = json_decode($app->request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("pastwishlist", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product information updated successfully.";
    echoResponse(200, $rows);
});

$app->delete('/pastwishlist/:id', function($id) {
    global $db;
    $rows = $db->delete("pastwishlist", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Product removed successfully.";
    echoResponse(200, $rows);
});

//Checkout
$app->get('/checkout', function() { 
    global $db;
    $rows = $db->select("checkout","id,name,price,year,company,rating,genre,platform,players,stock,copiessold,status",array());
    echoResponse(200, $rows);
});

$app->post('/checkout', function() use ($app) { 
    $data = json_decode($app->request->getBody());
    $mandatory = array('name');
    global $db;
    $rows = $db->insert("checkout", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product added successfully.";
    echoResponse(200, $rows);
});

$app->put('/checkout/:id', function($id) use ($app) { 
    $data = json_decode($app->request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("checkout", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product information updated successfully.";
    echoResponse(200, $rows);
});

$app->delete('/checkout/:id', function($id) { 
    global $db;
    $rows = $db->delete("checkout", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Product removed successfully.";
    echoResponse(200, $rows);
});


//Transaction
$app->get('/transaction', function() { 
    global $db;
    $rows = $db->select("transaction","transId",array());
    echoResponse(200, $rows);
});

$app->post('/transaction', function() use ($app) { 
    $data = json_decode($app->request->getBody());
    $mandatory = array('name');
    global $db;
    $rows = $db->insert("transaction", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product added successfully.";
    echoResponse(200, $rows);
});

$app->put('/transaction/:id', function($id) use ($app) { 
    $data = json_decode($app->request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("transaction", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product information updated successfully.";
    echoResponse(200, $rows);
});

$app->delete('/transaction/:id', function($id) { 
    global $db;
    $rows = $db->delete("transaction", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Product removed successfully.";
    echoResponse(200, $rows);
});

//PastShoppingCart
$app->get('/pastshop', function() { 
    global $db;
    $rows = $db->select("pastshop", "email,cardtype,creditcardname,creditcardnumber,id,name,price,year,company,rating,genre,platform,players,stock,copiessold,status", array());
    echoResponse(200, $rows);
}); //projection query since password column is removed.

$app->get('/pastshop/gamecount', function() { 
    global $db;
    $rows = $db->select("pastshop", "COUNT(*) as totalgames", array());
    echoResponse(200, $rows);
}); //regular aggregation query

//Get all games that have been wishlisted and those that were bought
//SELECT products.id FROM products INNER JOIN pastshop INNER JOIN pastwishlist ON products.id = pastshop.id AND pastshop.id = pastwishlist.id
//This is a division query using Inner Join following this example: http://stackoverflow.com/questions/22082215/dividing-two-fields-from-two-different-tables
$app->get('/pastshop/allGamesWishlistedBought', function() { 
    global $db;
    $from = "(SELECT products.id, products.name FROM products INNER JOIN pastshop INNER JOIN pastwishlist ON products.id = pastshop.id AND pastshop.id = pastwishlist.id) as temp";
    $rows = $db->select($from, "DISTINCT id, name", array());
    echoResponse(200, $rows);
});

$app->get('/pastshop/salesByGenre', function() { 
    global $db;
    $rows = $db->select_groupby("pastshop", "genre,avg(price) as avgPrice", array(), "genre");
    echoResponse(200, $rows);
}); //nested aggregate with group by

$app->get('/pastshop/salesByGenre/maxPrice', function() { 
    global $db;
    $from = "(SELECT genre, avg(price) as averagePrice FROM `pastshop` group by genre) as temp";
    $rows = $db->select($from, "genre,max(averagePrice) as maxPrice", array());
    echoResponse(200, $rows);
}); //nested aggregate with group by

$app->get('/pastshop/salesByGenre/minPrice', function() { 
    global $db;
    $from = "(SELECT genre, avg(price) as averagePrice FROM `pastshop` group by genre) as temp";
    $rows = $db->select($from, "genre,min(averagePrice) as minPrice", array());
    echoResponse(200, $rows);
}); //nested aggregate with group by

$app->post('/pastshop', function() use ($app) {
    $data = json_decode($app->request->getBody());
    $mandatory = array('name');
    global $db;
    $rows = $db->insert("pastshop", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product added successfully.";
    echoResponse(200, $rows);
});

$app->put('/pastshop/:id', function($id) use ($app) {
    $data = json_decode($app->request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("pastshop", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product information updated successfully.";
    echoResponse(200, $rows);
});

$app->delete('/pastshop/:id', function($id) {
    global $db;
    $rows = $db->delete("pastshop", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Product removed successfully.";
    echoResponse(200, $rows);
});

function echoResponse($status_code, $response) {
    global $app;
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response,JSON_NUMERIC_CHECK);
}

// Getting the join table for myAccount
$app->get('/pastshop/allPastshopPastwishlist', function() {
    global $db;
    $rows = $db->joinPastshopPastwishlist();
    echoResponse(200, $rows);

});

$app->run();
?>