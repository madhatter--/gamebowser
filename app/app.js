var app = angular.module('myApp', ['ngRoute', 'ui.bootstrap', 'ngAnimate']);

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
    .when('/', {
      title:'Main',
      controller: 'MainController',
      templateUrl: 'partials/main.html'
    })
    .when('/customer', {
      title:'Customer',
      controller: 'MainController',
      templateUrl: 'partials/menu.html'
    })
    .when('/owner', {
      title:'Owner',
      controller: 'MainController',
      templateUrl: 'partials/owner.html'
    })
    .when('/pastshoppingcart', {
      title:'PastShoppingCart',
      controller: 'MainController',
      templateUrl: 'partials/pastshoppingcart.html'
    })
    .when('/pastwishlist', {
      title:'PastWishList',
      controller: 'MainController',
      templateUrl: 'partials/pastwishlist.html'
    })
    .when('/products', {
      title: 'Products',
      controller: 'productsCtrl',
      templateUrl: 'partials/products.html'
    })
    .when('/search', {
      title: 'Search',
      controller: 'GameSearchController',
      templateUrl: 'partials/search.html'
    })
    .when('/list/shopping-cart', {
      title: 'My Shopping Cart',
      controller: 'MyShoppingCartController',
      templateUrl: 'partials/myShoppingCart.html'
    })
    .when('/list/wishlist', {
      title: 'My Wishlist',
      controller: 'MyWishlistController',
      templateUrl: 'partials/myWishlist.html'
    })
    .when('/checkout', {
      title: 'Checkout',
      controller: 'CheckoutController',
      templateUrl: 'partials/checkout.html'
    })
    .when('/account', {
      title: 'MyAccount',
      controller: 'MyAccountController',
      templateUrl: 'partials/account.html'
    })
    .when('/accountTable',{
      title: 'Account Table',
      controller: 'MyAccountController',
      templateUrl: 'partials/accountTable.html'
    })
    .when('/transactions', {
      title: 'Transactions',
      controller: 'TransactionsController',
      templateUrl: 'partials/transactions.html'
    })
    .when('/ownertools', {
      title: 'OwnerTools',
      controller: 'OwnerToolsController',
      templateUrl: 'partials/ownertools.html'
    })
    .when('/sales', {
      title: 'Sales',
      controller: 'SalesController',
      templateUrl: 'partials/sales.html'
    })
    .otherwise({
      redirectTo: '/'
    });
}]);
    