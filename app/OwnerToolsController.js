app.controller('OwnerToolsController', function ($scope, $modal, $filter, Data) {
    $scope.product = {};
    Data.get('/pastshop/gamecount').then(function(data){
        $scope.gamecount = data.data;
    });
    Data.get('/pastshop/allGamesWishlistedBought').then(function(data){
        $scope.gamesWishBought = data.data;
    });

    $scope.getNumberOfGamesSold = function() {
        document.getElementById("gamesSold").innerHTML = $scope.gamecount[0].totalgames;
    };

    $scope.getallBoughtWishGames = function() {
        var length = $scope.gamesWishBought.length;
        for(var i=0; i<length; i++) {
            document.getElementById("boughtWishGames").innerHTML = $scope.gamecount[i];
        }
    };


$scope.columns = [
                    {text:"ID",predicate:"id",sortable:true},
                    {text:"Name",predicate:"name",sortable:true}
                ];

});