app.controller('SalesController', function ($scope, $modal, $filter, Data) {
    $scope.product = {};
    Data.get('pastshop/salesByGenre').then(function(data){
        $scope.sales = data.data;
        console.log("avg: ", $scope.sales); //testing
    });
    
    $scope.columns = [
                    {text:"Genre",predicate:"genre",sortable:true},
                    {text:"Average Price",predicate:"avgPrice",sortable:true}
                ];

    $scope.maxButtonText = "View Max Price";
    $scope.minButtonText = "View Min Price";

    $scope.getMax = function(){
        Data.get('pastshop/salesByGenre/maxPrice').then(function(data){
            $scope.max = data.data;
        });

        $scope.displayMax = !$scope.displayMax;
        $scope.maxButtonText = ($scope.displayMax == true) ? "Hide Max Price" : "View Max Price";
    }

    $scope.getMin = function(){
        Data.get('pastshop/salesByGenre/minPrice').then(function(data){
            $scope.min = data.data;
        });

        $scope.displayMin = !$scope.displayMin;
        $scope.minButtonText = ($scope.displayMin == true) ? "Hide Min Price" : "View Min Price";
    }

});