app.controller('GameSearchController', function ($scope, $modal, $filter, Data) {
    $scope.product = {};
    Data.get('products').then(function(data){
        $scope.products = data.data;
    });
    
    $scope.columns = [
                    {text:"ID",predicate:"id",sortable:true,dataType:"number"},
                    {text:"Name",predicate:"name",sortable:true},
                    {text:"Price",predicate:"price",sortable:true},
                    {text:"Year",predicate:"year",sortable:true},
                    {text:"Company",predicate:"company",sortable:true},
                    {text:"Rating",predicate:"rating",reverse:true,sortable:true,dataType:"number"},
                    {text:"Genre",predicate:"genre",sortable:true},
                    {text:"Platform",predicate:"platform",sortable:true},
                    {text:"Players",predicate:"players",sortable:true},
                    {text:"Stock",predicate:"stock",sortable:true},
                    {text:"Copies Sold",predicate:"copiessold",sortable:true},
                    {text:"Action",predicate:"",sortable:false}
                ];

    $scope.addToCart = function (product) {
        product.uid = $scope.uid;
        Data.post('shop', product).then(function (result) {
            if(result.status != 'error'){
                alert(product.name + "was added to your shopping cart.");
                var x = angular.copy(product);
                x.save = 'insert';
                x.id = result.data;
            }else{
                alert(product.name + "is already in your shopping cart.");
                console.log(result);
            }
        });
    };

    $scope.addToWishlist = function (product) {
        product.uid = $scope.uid;
        Data.post('wishlist', product).then(function (result) {
            if(result.status != 'error'){
                alert(product.name + "was added to your wishlist.");
                var x = angular.copy(product);
                x.save = 'insert';
                x.id = result.data;
            }else{
                alert(product.name + "is already in your wishlist.");
                console.log(result);
            }
        });
    };

});
