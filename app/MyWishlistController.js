app.controller('MyWishlistController', function ($scope, $modal, $filter, Data) {
  $scope.product = {};
  Data.get('wishlist').then(function(data){
      $scope.wishlist = data.data;
  });

  Data.get('pastwishlist').then(function(data){
    $scope.pastwishlist = data.data;
  });

  $scope.changeProductStatus = function(product){
      product.status = (product.status=="Purchase" ? "Rent" : "Purchase");
      Data.put("wishlist/"+product.id,{status:product.status});
  };

  $scope.deleteProduct = function(product){
      if(confirm("Are you sure to remove the product")){
          Data.delete("wishlist/"+product.id).then(function(result){
              $scope.wishlist = _.without($scope.wishlist, _.findWhere($scope.wishlist, {id:product.id}));
          });
      }
  };

  // Once logged in, should add the games in wishlist to pastwishlist table, using the customer's email and password as key
  $scope.customerCheckoutLogin = function(product) {
    var z = document.forms['loginform']['customeremail'].value;
    var y = document.forms['loginform']['customerpassword'].value;
    if((z==null || z=="") || (y==null || y=="")) {
      alert("Please enter your email and password.");
      return false;
    }
    else {

        for(var key in $scope.wishlist){
          var obj = $scope.wishlist[key];
        obj.email = z;
        obj.password = y;
        console.log(obj);
        Data.post('pastwishlist', {
          email: obj.email,
          password: obj.password,
          id: obj.id,
          name: obj.name,
          price: obj.price,
          year: obj.year,
          company: obj.company,
          rating: obj.rating,
          genre: obj.genre,
          platform: obj.platform,
          players: obj.players,
          stock: obj.stock,
          copiessold: obj.copiessold,
          status: obj.status
        }).then(function (result) {
          if(result.status != 'error'){
            //var x = angular.copy(product);
            //x.save = 'insert';
            //x.id = result.data;
            //$modalInstance.close(x);
          }else{
            console.log(result);
          }
        });
      };

    }
  };

  $scope.saveProduct = function (product) {
    product.uid = $scope.uid;
    if(product.id > 0){
      Data.put('products/'+product.id, product).then(function (result) {
        if(result.status != 'error'){
          var x = angular.copy(product);
          x.save = 'update';
          $modalInstance.close(x);
        }else{
          console.log(result);
        }
      });
    }else{
      Data.post('products', product).then(function (result) {
        if(result.status != 'error'){
          var x = angular.copy(product);
          x.save = 'insert';
          x.id = result.data;
          $modalInstance.close(x);
        }else{
          console.log(result);
        }
      });
    }
  };

  $scope.columns = [
                    {text:"ID",predicate:"id",sortable:true,dataType:"number"},
                    {text:"Name",predicate:"name",sortable:true},
                    {text:"Price",predicate:"price",sortable:true},
                    {text:"Year",predicate:"year",sortable:true},
                    {text:"Company",predicate:"company",sortable:true},
                    {text:"Rating",predicate:"rating",reverse:true,sortable:true,dataType:"number"},
                    {text:"Genre",predicate:"genre",sortable:true},
                    {text:"Platform",predicate:"platform",sortable:true},
                    {text:"Players",predicate:"players",sortable:true},
                    {text:"Stock",predicate:"stock",sortable:true},
                    {text:"Copies Sold",predicate:"copiessold",sortable:true},
                    {text:"Action",predicate:"",sortable:false}
                ];

});
