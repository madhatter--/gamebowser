app.controller('TransactionsController', function ($scope, $modal, $filter, Data) {
    $scope.product = {};
    Data.get('pastshop').then(function(data){
        $scope.transactions = data.data;
    });
    
$scope.columns = [
                    {text:"Email",predicate:"email",sortable:true},
                    {text:"Name",predicate:"name",sortable:true},
                    {text:"Price",predicate:"price",sortable:true},
                    {text:"Year",predicate:"year",sortable:true},
                    {text:"Company",predicate:"company",sortable:true},
                    {text:"Rating",predicate:"rating",sortable:true,dataType:"number"},
                    {text:"Genre",predicate:"genre",sortable:true},
                    {text:"Platform",predicate:"platform",sortable:true},
                    {text:"Players",predicate:"players",sortable:true},
                    {text:"Stock",predicate:"stock",sortable:true},
                    {text:"Copies Sold",predicate:"copiessold",sortable:true},
                    {text:"Status",predicate:"status",sortable:true}
                ];

});