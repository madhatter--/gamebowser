app.controller('CheckoutController', function($scope, $routeParams, Data){
	$scope.product = {};
    Data.get('checkout').then(function(data){
        $scope.checkout = data.data;
    });

    $scope.customerCheckoutLogin = function() {

    	var z = document.forms['loginform']['customeremail'].value;
    	var y = document.forms['loginform']['customerpassword'].value;
    	if((z==null || z=="") || (y==null || y=="")) {
    		alert("Please enter your email and password.");
    		return false;
    	}
    	else {
    		$("#paymentArea").css("display", "block");
    	}
    };

    //Grabs the entire list of games and puts it in an array
    var checkoutlist = [];
    var checkoutlistIndex = 0;
    $scope.getProductInformation = function(product) {
    	if(product.status == "Purchase" || product.status == "Rent") {
    		checkoutlist[checkoutlistIndex] = product;
    		checkoutlistIndex++;
    	}
    };

    //When cancel is clicked, clear entire list in checkout
    var index = 0;
    $scope.dropFromCheckout = function() {
    	if(confirm("Cancel Transaction?")){
    		for(var i=0; i<checkoutlist.length; i++) {
    			Data.delete("checkout/"+checkoutlist[i].id).then(function(result){
                	$scope.checkout = _.without($scope.checkout, _.findWhere($scope.checkout, {id:checkoutlist[i].id}));
            	});
    		}
        }
    };

    //Get total price
    var totalPrice = 0;
    var executed = false;
    $scope.getTotalPrice = function() {
    	if(executed === false || totalPrice === 0) {
    		for(var i=0; i<checkoutlist.length; i++) {
    			totalPrice = totalPrice + checkoutlist[i].price;
    		}

    		//This makes sure this function is only executed once
    		executed = true;
    	}

    	document.getElementById("totalPrice").innerHTML = "$"+totalPrice;
    };

    //Confirm purchase
    $scope.confirmPurchase = function() {
    	var x = document.forms['transactionform']['creditcardnumber'].value;
    	if(x==null || x=="") {
    		alert("Please enter a valid credit card number.");
    		return false;
    	}

    	var y = document.forms['transactionform']['cardtypeselect'].value;
    	if(y=="--") {
    		alert("Please select your creditcard type.");
    		return false;
    	}
    };

    //COmplete Transaction
    $scope.completeTransaction = function() {
    	var customerEmail = document.getElementById("customeremail").value;
    	var customerPassword = document.getElementById("customerpassword").value;

    	//For card type
    	var sel = document.getElementById("cardtypeselect");
		var value = sel.options[sel.selectedIndex].value;
		var text = sel.options[sel.selectedIndex].text;

    	var cardName = document.getElementById("creditcardname").value;
    	var cardNumber = document.getElementById("creditcardnumber").value;

    	for(var i=0; i<checkoutlist.length; i++) {
			Data.post('pastshop', {email:customerEmail, password:customerPassword, cardtype:text, creditcardname:cardName, creditcardnumber: cardNumber,
				id: checkoutlist[i].id,name: checkoutlist[i].name,price: checkoutlist[i].price,year: checkoutlist[i].year,
                company: checkoutlist[i].company,rating: checkoutlist[i].rating,genre: checkoutlist[i].genre,platform: checkoutlist[i].platform,
                players: checkoutlist[i].players,stock: checkoutlist[i].stock,copiessold: checkoutlist[i].copiessold,status: checkoutlist[i].status}).then(function (result) {
                if(result.status != 'error'){
                    var x = checkoutlist[i];
                    x.save = 'insert';
                    x.id = result.data;
                }else{
                    console.log(result);
                }
            });

            if(i === (checkoutlist.length-1)) {
            	$scope.removeCheckoutItems();
            }
    	}

    	window.location.reload();
    };

    $scope.removeCheckoutItems = function() {
    	alert("Transaction Complete. Thank you, come again!");
    	//Need to clear out checkoutlist and take them back to main menu
    	for(var j=0; j<checkoutlist.length; j++) {
    		Data.delete("checkout/"+checkoutlist[j].id).then(function(result){
              	$scope.checkout = _.without($scope.checkout, _.findWhere($scope.checkout, {id:checkoutlist[j].id}));
            });
    	}
    };

    //when confirming transaction, confirm that the card number is valid
 $scope.columns = [
                    {text:"ID",predicate:"id",sortable:true,dataType:"number"},
                    {text:"Name",predicate:"name",sortable:true},
                    {text:"Price",predicate:"price",sortable:true},
                    {text:"Year",predicate:"year",sortable:true},
                    {text:"Company",predicate:"company",sortable:true},
                    {text:"Rating",predicate:"rating",reverse:true,sortable:true,dataType:"number"},
                    {text:"Genre",predicate:"genre",sortable:true},
                    {text:"Platform",predicate:"platform",sortable:true},
                    {text:"Players",predicate:"players",sortable:true},
                    {text:"Stock",predicate:"stock",sortable:true},
                    {text:"Copies Sold",predicate:"copiessold",sortable:true},
                    {text:"Action",predicate:"",sortable:false}
                ];
});