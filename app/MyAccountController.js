app.controller('MyAccountController', function($scope, $location ,$routeParams, Data){

  var z;
  var y;

  $scope.joinTable = [];

  Data.get('/pastshop').then(function(data){
    $scope.pastshop = data.data;
  });

  Data.get('/pastwishlist').then(function(data){
    $scope.pastwishlist = data.data;
  });

  Data.get('/pastshop/allPastshopPastwishlist').then(function(data){
    $scope.allPastshopPastwishlist = data.data;
  });



  $scope.customerCheckoutLogin = function() {
    z = document.forms['loginform']['customeremail'].value;
    y = document.forms['loginform']['customerpassword'].value;
    if ((z == null || z == "") || (y == null || y == "")) {
      alert("Please enter your email and password.");
      return false;
    }
    else {
      $location.path( "/accountTable" );
      console.log(z);
      console.log(y);

      //Show only the objects for the logged in user.

      for(var key in $scope.allPastshopPastwishlist) {
        var obj = $scope.allPastshopPastwishlist[key];

        console.log(obj);
        if(obj.email == z && obj.password == y){
          $scope.joinTable.push(obj);
        };

      };
      console.log($scope.joinTable);

    }
  };

  $scope.columns = [
    {text:"Id",predicate:"id",sortable:true},
    {text:"Name",predicate:"name",sortable:true},
    {text:"Price",predicate:"price",sortable:true},
    {text:"Year",predicate:"year",sortable:true},
    {text:"Company",predicate:"company",sortable:true},
    {text:"Rating",predicate:"rating",sortable:true,dataType:"number"},
    {text:"Genre",predicate:"genre",sortable:true},
    {text:"Platform",predicate:"platform",sortable:true},
    {text:"Players",predicate:"players",sortable:true}
  ];

});