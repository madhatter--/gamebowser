app.controller('MyShoppingCartController', function ($scope, $modal, $filter, Data) {
    $scope.product = {};
    Data.get('shop').then(function(data){
        $scope.shop = data.data;
    });

    var cart = [];
    var cartIndex = 0;
    $scope.changeProductStatus = function(product){
        product.status = (product.status=="Purchase" ? "Rent" : "Purchase");
        Data.put("shop/"+product.id,{status:product.status});

        if(product.status == "Purchase") {
            cart[cartIndex] = product;
            cartIndex++;
        }
        else if(product.status == "Rent"){
            cart[cartIndex] = product;
            cartIndex++;
        }
    };

    $scope.deleteProduct = function(product){
        if(confirm("Are you sure you want to remove this product?")){
            Data.delete("shop/"+product.id).then(function(result){
                $scope.shop = _.without($scope.shop, _.findWhere($scope.shop, {id:product.id}));
            });
        }
    };

    $scope.checkoutShoppingCart = function() {
        for(i=0; i<cart.length; i++) {
            var product = angular.copy(cart[i]);
            Data.post('checkout', {id: product.id,name: product.name,price: product.price,year: product.year,
                company: product.company,rating: product.rating,genre: product.genre,platform: product.platform,
                players: product.players,stock: product.stock,copiessold: product.copiessold,status: product.status}).then(function (result) {
                if(result.status != 'error'){
                    var x = product;
                    x.save = 'insert';
                    x.id = result.data;
                }else{
                    console.log(result);
                }
            });
        }
    };
    
 $scope.columns = [
                    {text:"ID",predicate:"id",sortable:true,dataType:"number"},
                    {text:"Name",predicate:"name",sortable:true},
                    {text:"Price",predicate:"price",sortable:true},
                    {text:"Year",predicate:"year",sortable:true},
                    {text:"Company",predicate:"company",sortable:true},
                    {text:"Rating",predicate:"rating",reverse:true,sortable:true,dataType:"number"},
                    {text:"Genre",predicate:"genre",sortable:true},
                    {text:"Platform",predicate:"platform",sortable:true},
                    {text:"Players",predicate:"players",sortable:true},
                    {text:"Stock",predicate:"stock",sortable:true},
                    {text:"Copies Sold",predicate:"copiessold",sortable:true},
                    {text:"Action",predicate:"",sortable:false}
                ];

});
